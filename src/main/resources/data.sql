 -- IGNORE means to ignore if duplicate value exists
  INSERT IGNORE INTO user (username, password, email, description, registration_date, active, display_name, role, roles)
  VALUES ('admin', '$2a$10$j/Q7pUKgiVaz/ocNLHbiFu56s9dcN2O/helsUO97WgwhnT64zhP9y', 'admin123@gmail.com', 'description', '2022-05-18', true, 'Admin', 'User', 'ADMIN');
