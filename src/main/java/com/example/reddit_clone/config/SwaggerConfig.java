package com.example.reddit_clone.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

// http://localhost:8080/v2/api-docs
// http://localhost:8080/swagger-ui/index.html
//   .requestMatchers("/swagger-ui/**", "/v3/api-docs/**").permitAll()
@Configuration
@EnableSwagger2
public class SwaggerConfig  {

    public static final String POST_TAG = "posts";
    public static final String COMMUNITY_TAG = "community";
    public static final String COMMENTS_TAG = "comment";
    public static final String REACTION_TAG = "reaction" ;

    public static final String REPORT_TAG = "report" ;
    public static final String USER_TAG = "user" ;

    public static final String BANNED = "banned" ;


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.reddit_clone"))
                .paths(PathSelectors.any())
                .build().tags(

                        new Tag(POST_TAG, "Posts"),
                        new Tag(COMMUNITY_TAG, "Comunities"),
                        new Tag(COMMENTS_TAG, "Comments"),
                        new Tag(REACTION_TAG, "Reactions"),
                        new Tag(REPORT_TAG, "Reports"),
                        new Tag(USER_TAG, "Posts"),
                        new Tag(BANNED, "Banned")
                        );
    }
}