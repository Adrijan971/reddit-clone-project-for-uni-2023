package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.RuleDTO;
import com.example.reddit_clone.model.Rule;

import java.util.List;

public interface RuleService {

    Rule findOneById(Integer id);

    List<Rule> findAll();

    // This one by default saves User object without filtering through DTO.
    Rule save(Rule rule);

    // This one will save DTO of an User object.
    Rule createRule(RuleDTO ruleDto);

}
