package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.CommentDTO;
import com.example.reddit_clone.dto.FlairDTO;
import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Flair;

import java.util.List;

public interface FlairService {

    Flair findOneById(Integer id);
    List<Flair> findAll();

    Flair save(Flair flair);

    Flair createFlair(FlairDTO flairDTO);

}
