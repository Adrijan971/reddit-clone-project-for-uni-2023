package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {

    Post getOne(Integer id);
    List<Post> findAll();
    Post save(Post post);
    Post createPost(PostDTO postDTO);

    List<Post> searchByTitle(String searchText);

    List<Post> searchByDescription(String searchText);

    List<Post> searchByPDFDescription(String searchText);

    List<PostDTO> searchByKarmaRange(int minKarma , int maxKarma);

    PostDTO setKarmaForPostDto(PostDTO postDto);

    void setReactionByAuthUser(List<PostDTO> dtos);

}
