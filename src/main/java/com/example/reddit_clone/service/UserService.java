package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.UserDTO;
import com.example.reddit_clone.model.User;

import java.util.List;

public interface UserService {

    User findOne(String username);
    User findOneById(Integer id);
    List<User> findAll();

    // This one by default saves User object without filtering through DTO.
    User save(User user);

    // This one will save DTO of an User object.
    User createUser(UserDTO userDTO);

    UserDTO setKarmaForUserDto(UserDTO userDTO);

    UserDTO setCommentListForUserDto(UserDTO userDTO);

    void delete(int id);

}
