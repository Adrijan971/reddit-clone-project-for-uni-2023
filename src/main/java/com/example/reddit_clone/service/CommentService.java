package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.CommentDTO;
import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.model.Comment;

import java.util.List;

public interface CommentService {

    Comment findOneById(Integer id);
    List<Comment> findAll();

    Comment save(Comment comment);

    Comment createComment(CommentDTO commentDTO);

    void setReactionByAuthUser(List<CommentDTO> dtos);


}
