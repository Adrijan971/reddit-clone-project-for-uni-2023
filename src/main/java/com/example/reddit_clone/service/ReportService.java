package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.ReportDTO;
import com.example.reddit_clone.dto.UserDTO;
import com.example.reddit_clone.model.Report;
import com.example.reddit_clone.model.User;

import java.util.List;

public interface ReportService {

    Report findOneById(Integer id);

    List<Report> findAll();

    // This one by default saves User object without filtering through DTO.
    Report save(Report report);

    // This one will save DTO of an User object.
    Report createReport(ReportDTO reportDto);

}
