package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.BannedDTO;
import com.example.reddit_clone.model.Banned;

import java.util.List;

public interface BannedService {

    Banned findOneById(Integer id);
    List<Banned> findAll();

    Banned save(Banned banned);

    Banned createBanned(BannedDTO bannedDTO);

}
