package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.RuleDTO;
import com.example.reddit_clone.model.Rule;
import com.example.reddit_clone.repository.RuleRepository;
import com.example.reddit_clone.service.CommunityService;
import com.example.reddit_clone.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RuleServiceImplementation implements RuleService {

    @Autowired
    RuleRepository ruleRepository;

    @Autowired
    CommunityService communityService;

    @Override
    public Rule findOneById(Integer id) {
        Optional<Rule> rule = ruleRepository.findById(id);
        if (rule.isPresent()) {
            return rule.get();
        }

        return null;
    }

    @Override
    public List<Rule> findAll() {
        return ruleRepository.findAll();
    }

    @Override
    public Rule save(Rule rule) {
        try{
            return ruleRepository.save(rule);
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    @Override
    public Rule createRule(RuleDTO ruleDto) {

        Rule rule = new Rule();
        rule.setRuleId(ruleDto.getRuleId());
        rule.setActive(true);
        rule.setDescription(ruleDto.getDescription());
        rule.setCommunity(communityService.getOne(ruleDto.getCommunity()));

        ruleRepository.save(rule);

        return rule;
    }
}
