package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.CommentDTO;
import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.dto.UserDTO;
import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.model.enums.ReactionType;
import com.example.reddit_clone.model.enums.Roles;
import com.example.reddit_clone.repository.CommentRepository;
import com.example.reddit_clone.repository.ReactionRepository;
import com.example.reddit_clone.repository.UserRepository;
import com.example.reddit_clone.service.CommentService;
import com.example.reddit_clone.service.ReactionService;
import com.example.reddit_clone.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImplementation implements UserService, UserDetailsService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    ReactionRepository reactionRepository;

    @Autowired
    CommentRepository commentRepository;

    // circular with SecurityService
    private PasswordEncoder passwordEncoder;

    @Autowired
    public  UserServiceImplementation(PasswordEncoder pe) {
        this.passwordEncoder = pe;
    }

    @Override
    public User findOne(String username) {
        Optional<User> user = userRepository.findFirstByUsername(username);
        if (!user.isEmpty()) {
            return user.get();
        }
        return null;
    }

    @Override
    public User findOneById(Integer id) {
        Optional<User> user = userRepository.findFirstById(id);
        if (user.isPresent()) {
            return user.get();
        }

        return null;
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public User save(User user) {
        try{
            return userRepository.save(user);
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    @Override
    public User createUser(UserDTO userDTO) {
    //    Optional<User> user = userRepository.findFirstByUsername(userDTO.getUsername());
    //
    //    if(user.isPresent()){
    //        return null;
    //    }

        User newUser = new User();
        newUser.setUsername(userDTO.getUsername());
        
        // HASH THE PASSWORD, we don't store it as plain text
        newUser.setPassword(passwordEncoder.encode(userDTO.getPassword()));

        newUser.setEmail(userDTO.getEmail());
        newUser.setAvatar("");
        newUser.setRegistrationDate(new Date());
        newUser.setDescription(userDTO.getDescription());
        newUser.setDisplayName(userDTO.getDisplayName());
        newUser.setRoles(Roles.USER);
        newUser.setActive(true);
        newUser = userRepository.save(newUser);

        return newUser;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        User user = findOne(username);

        if(user == null){
            throw new UsernameNotFoundException("Invalid username");
        }else{
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            String role = "ROLE_" + user.getRoles().toString();
            grantedAuthorities.add(new SimpleGrantedAuthority(role));
    //    //@PreAuthorize("hasRole('ROLE_MODERATOR')")
            //    //@PreAuthorize("hasRole('ROLE_MODERATOR')")

            //    //@PreAuthorize("hasRole('ROLE_ADMIN')")

            return new org.springframework.security.core.userdetails.User(
                    user.getUsername().trim(),
                    user.getPassword().trim(),
                    grantedAuthorities);
        }
    }

    public UserDTO setKarmaForUserDto(UserDTO userDTO){

        int karma = 0;

        List<Reaction> list = reactionRepository.findByPostId(userDTO.getUserId());

        for (Reaction r : list){
            if (r.getType() == ReactionType.UPVOTE){
                karma = karma + 1;
            }
            else if (r.getType() == ReactionType.DOWNVOTE){
                karma = karma - 1;
            }
        }

        userDTO.setKarma(karma);

        return userDTO;
    }

    @Override
    public UserDTO setCommentListForUserDto(UserDTO userDTO) {

        List<Comment> commentList = commentRepository.findByUserId(userDTO.getUserId());
        List<CommentDTO> commentDTOList = new ArrayList<>();

        for (Comment comment : commentList){
            commentDTOList.add(new CommentDTO(comment));
        }

        userDTO.setCommentList(commentDTOList);

        return userDTO;
    }

}
