package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.dto.ReactionDTO;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.model.enums.ReactionType;
import com.example.reddit_clone.repository.PostRepository;
import com.example.reddit_clone.elastic.elasticsearch.ElasticPostRepository;
import com.example.reddit_clone.repository.ReactionRepository;
import com.example.reddit_clone.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostServiceImplementation implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private ElasticPostRepository elasticPostRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private FlairService flairService;

    @Autowired
    private CommunityService communityService;

    private final ReactionService reactionService;

    @Autowired
    public PostServiceImplementation(ReactionService reactionService){
        this.reactionService = reactionService;
    }

    @Override
    public Post getOne(Integer id) {

        Optional<Post> postOpt = postRepository.findFirstById(id);

        if (postOpt.isPresent()){
            return postOpt.get();
        }

        return null;
    }

    @Override
    public List<Post> findAll() {
        return postRepository.findAll();
    }

    @Override
    public Post save(Post post) {

        elasticPostRepository.save(post);
        return postRepository.save(post);
    }

    @Override
    public Post createPost(PostDTO postDTO) {

        Post newPost = new Post();
        newPost.setTitle(postDTO.getTitle());
        newPost.setText(postDTO.getText());
        newPost.setCreationDate(new Date());
        newPost.setUser(userService.findOneById(postDTO.getUser()));
        newPost.setCommunity(communityService.getOne(postDTO.getCommunity()));
        newPost.setFlair(flairService.findOneById(postDTO.getFlair()));
        newPost.setImagePath(postDTO.getImagePath());
        newPost.setDescriptionPDF(postDTO.getPDFtext());
        newPost.setActive(true);


        postRepository.save(newPost);

        Post forElastic = new Post();
        forElastic.setId(newPost.getId());
        forElastic.setText(newPost.getText());
        forElastic.setTitle(newPost.getTitle());
        forElastic.setDescriptionPDF(newPost.getDescriptionPDF());

        elasticPostRepository.save(forElastic);


        User user = getAuthUser();
        if (user != null) {
            Reaction reaction = new Reaction();
            reaction.setType(ReactionType.UPVOTE);
            reaction.setUser(user);
            reaction.setPost(newPost);
            reactionService.createReaction(new ReactionDTO(reaction));
        }



//        Optional<Post> freshPost = postRepository.findById(newPost.getId());
//        if (freshPost.isPresent()) {
//            List<Reaction> reactions = reactionService.findByPostId(freshPost.get().getId());
//            return freshPost.get();
//        }
        return newPost;

    }

    @Override
    public List<Post> searchByTitle(String searchText) {
        List<Post> elasticPosts = elasticPostRepository.findAllByTitle(searchText);
        List<Integer> postIds = elasticPosts.stream()
                .map(Post::getId)
                .collect(Collectors.toList());

        List<Post> jpaPosts = postRepository.findAllById(postIds);
        return jpaPosts;
    }

    @Override
    public List<Post> searchByDescription(String searchText) {
        List<Post> elasticPosts = elasticPostRepository.findAllByText(searchText);
        List<Integer> postIds = elasticPosts.stream()
                .map(Post::getId)
                .collect(Collectors.toList());

        List<Post> jpaPosts = postRepository.findAllById(postIds);
        return jpaPosts;
    }

    @Override
    public List<Post> searchByPDFDescription(String searchText) {
        List<Post> elasticPosts = elasticPostRepository.findAllByDescriptionPDF(searchText);
        List<Integer> postIds = elasticPosts.stream()
                .map(Post::getId)
                .collect(Collectors.toList());

        List<Post> jpaPosts = postRepository.findAllById(postIds);

        return jpaPosts;
    }

    @Override
    public List<PostDTO> searchByKarmaRange(int minKarma, int maxKarma) {
        List<Post> posts = postRepository.findAll();

        //convert to dto's
        List<PostDTO> postDTOS = new ArrayList<>();
        for (Post post : posts){
            postDTOS.add(new PostDTO(post));
        }

        //calculate karma for each post
        for (PostDTO postDto : postDTOS){
            setKarmaForPostDto(postDto);
        }

        setReactionByAuthUser(postDTOS);

        // get ones in range
        List<PostDTO> filteredPostDTOS = new ArrayList<>();

        for (PostDTO postDto : postDTOS){
            if(postDto.getReactions_sum() >= minKarma && postDto.getReactions_sum() <= maxKarma){
                filteredPostDTOS.add(postDto);
            }
        }

        return filteredPostDTOS;
    }

    //calculate karma for per post and set/update it as attribute
    public PostDTO setKarmaForPostDto(PostDTO postDto){

        int karma = 0;

        List<Reaction> list = reactionService.findByPostId(postDto.getPostId());

        for (Reaction r : list){
            if (r.getType() == ReactionType.UPVOTE){
                karma = karma + 1;
            }
            else if (r.getType() == ReactionType.DOWNVOTE){
                karma = karma - 1;
            }
        }

        postDto.setReactions_sum(karma);

        return postDto;
    }

    public User getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // Check if the user is authenticated
        if (authentication != null && authentication.isAuthenticated()
        &&  authentication.getPrincipal() instanceof UserDetails) {
            // Get the username or other user details
            UserDetails user = (UserDetails) authentication.getPrincipal();
            User userDetails = userService.findOne(user.getUsername());
            return userDetails;
        } else {
            return null;
        }
    }

    public void setReactionByAuthUser(List<PostDTO> dtos) {

        User user = getAuthUser();
        if (user == null) {
            return;
        }

        List<Integer> postIds = dtos.stream()
                .map(PostDTO::getPostId)
                .collect(Collectors.toList());

        List<Reaction> reactions = reactionService.findByUserIdAndPostIdIn(user.getId(), postIds);


        // Make a map to get the reaction
        Map<Integer, Reaction> postIdReactionMap = reactions.stream()
                .filter(reaction -> postIds.contains(reaction.getPost().getId())) // Filter by postIds
                .collect(Collectors.toMap(reaction -> reaction.getPost().getId(), reaction -> reaction));

        for(PostDTO postDTO: dtos) {

            if (postIdReactionMap.containsKey(postDTO.getPostId())) {
                Reaction reaction = postIdReactionMap.get(postDTO.getPostId());
                postDTO.setReactionByAuthUser(reaction.getType());

            }
        }
    }


}
