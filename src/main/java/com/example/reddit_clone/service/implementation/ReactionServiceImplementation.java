package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.ReactionDTO;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.repository.ReactionRepository;
import com.example.reddit_clone.service.CommentService;
import com.example.reddit_clone.service.PostService;
import com.example.reddit_clone.service.ReactionService;
import com.example.reddit_clone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReactionServiceImplementation implements ReactionService {

    @Autowired
    ReactionRepository reactionRepository;


    @Autowired
    @Lazy
    CommentService commentService;

    @Autowired
    @Lazy
    PostService postService;

    @Autowired
    UserService userService;

//    @Autowired
//    public ReactionServiceImplementation(CommentService commentService){
//
//        this.commentService = commentService;
//    }

    @Override
    public Reaction findOneById(Integer id) {
        Optional<Reaction> reactionOpt = reactionRepository.findFirstByReactionId(id);

        if (reactionOpt.isPresent()){
            return reactionOpt.get();
        }

        return null;
    }

    @Override
    public List<Reaction> findAll() {
        return reactionRepository.findAll();
    }

    @Override
    public List<Reaction> findByUserIdAndPostIdIn(Integer userId, List<Integer> postIds) {
        return reactionRepository.findByUserIdAndPostIdIn(userId, postIds);
    }

    @Override
    public List<Reaction> findByUserIdAndCommentIdIn(Integer userId, List<Integer> commentIds) {
        return reactionRepository.findByUserIdAndCommentIdIn(userId, commentIds);
    }

    @Override
    public Reaction save(Reaction reaction) {
        try{
            return reactionRepository.save(reaction);
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    @Override
    public Reaction createReaction(ReactionDTO reactionDTO) {

        Reaction reaction = new Reaction();
        User user = userService.findOneById(reactionDTO.getUserId());
        if (reactionDTO.getCommentId() != 0){
            List<Integer> commentId = new ArrayList<>();
            commentId.add(reactionDTO.getCommentId());
            List<Reaction> existing = reactionRepository.findByUserIdAndCommentIdIn(user.getId(), commentId);
            for(Reaction existingReaction: existing) {
                reactionRepository.delete(existingReaction);
            }
            reaction.setComment(commentService.findOneById(reactionDTO.getCommentId()));
        }
        if (reactionDTO.getPostId() != 0){
            List<Integer> postId = new ArrayList<>();
            postId.add(reactionDTO.getPostId());
            List<Reaction> existing = reactionRepository.findByUserIdAndPostIdIn(user.getId(), postId);
            for(Reaction existingReaction: existing) {
                reactionRepository.delete(existingReaction);
            }
            reaction.setPost(postService.getOne(reactionDTO.getPostId()));
        }
        reaction.setUser(user);
        reaction.setType(reactionDTO.getType());
        reaction.setActive(true);
        reaction.setCreationDate(new Date());

        reactionRepository.save(reaction);

        return reaction;

    }

    @Override
    public List<Reaction> findByPostId(Integer id) {
        return reactionRepository.findByPostId(id);
    }

}
