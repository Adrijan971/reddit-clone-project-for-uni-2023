package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.FlairDTO;
import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.Report;
import com.example.reddit_clone.repository.FlairRepository;
import com.example.reddit_clone.repository.ReportRepository;
import com.example.reddit_clone.service.FlairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FlairServiceImplementation implements FlairService {

    @Autowired
    FlairRepository flairRepository;

    @Override
    public Flair findOneById(Integer id) {
        Optional<Flair> flair = flairRepository.findById(id);
        if (flair.isPresent()) {
            return flair.get();
        }

        return null;
    }

    @Override
    public List<Flair> findAll() {
        return flairRepository.findAll();
    }

    @Override
    public Flair save(Flair flair) {
        try{
            return flairRepository.save(flair);
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    @Override
    public Flair createFlair(FlairDTO flairDTO) {

        Flair flair = new Flair();
        flair.setName(flairDTO.getName());
        flair.setActive(true);

        flairRepository.save(flair);

        return flair;
    }
}
