package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.BannedDTO;
import com.example.reddit_clone.model.Banned;
import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.repository.BannedRepository;
import com.example.reddit_clone.service.BannedService;
import com.example.reddit_clone.service.CommunityService;
import com.example.reddit_clone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BannedServiceImplementation implements BannedService {

    @Autowired
    BannedRepository bannedRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CommunityService communityService;

    @Override
    public Banned findOneById(Integer id) {
        Optional<Banned> bannedOpt = bannedRepository.findById(id);

        if (bannedOpt.isPresent()) {
            return bannedOpt.get();
        }

        return null;
    }

    @Override
    public List<Banned> findAll() {
        return bannedRepository.findAll();
    }

    @Override
    public Banned save(Banned banned) {
        return bannedRepository.save(banned);
    }

    @Override
    public Banned createBanned(BannedDTO bannedDTO) {

        Banned banned = new Banned();
        banned.setBannedReason(bannedDTO.getBannedReason());
        banned.setCreationDate(new Date());
        banned.setBy(userService.findOneById(bannedDTO.getCommunity()));
        banned.setCommunity(communityService.getOne(bannedDTO.getCommunity()));
        banned.setActive(true);

        bannedRepository.save(banned);
        return banned;
    }
}
