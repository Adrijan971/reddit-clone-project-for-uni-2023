package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.ReportDTO;
import com.example.reddit_clone.model.Report;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.repository.ReportRepository;
import com.example.reddit_clone.service.CommentService;
import com.example.reddit_clone.service.PostService;
import com.example.reddit_clone.service.ReportService;
import com.example.reddit_clone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReportServiceImplementation implements ReportService {

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    @Override
    public Report findOneById(Integer id) {
        Optional<Report> report = reportRepository.findById(id);
        if (report.isPresent()) {
            return report.get();
        }

        return null;
    }

    @Override
    public List<Report> findAll() {
        return reportRepository.findAll();
    }

    @Override
    public Report save(Report report) {
        try{
            return reportRepository.save(report);
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    @Override
    public Report createReport(ReportDTO reportDto) {

        Report report = new Report();
        report.setReason(reportDto.getReason());
        report.setCreationDate(new Date());
        report.setAccepted(false);
        report.setByUser(userService.findOneById(reportDto.getByUser()));
        if (reportDto.getPostId() != 0){
            report.setPost(postService.getOne(reportDto.getPostId()));
        }
        if (reportDto.getCommentId() != 0){
            report.setComment(commentService.findOneById(reportDto.getCommentId()));
        }

        reportRepository.save(report);

        return report;
    }
}
