package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.CommentDTO;
import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.dto.ReactionDTO;
import com.example.reddit_clone.dto.UserDTO;
import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.model.enums.ReactionType;
import com.example.reddit_clone.repository.CommentRepository;
import com.example.reddit_clone.repository.PostRepository;
import com.example.reddit_clone.repository.UserRepository;
import com.example.reddit_clone.service.CommentService;
import com.example.reddit_clone.service.ReactionService;
import com.example.reddit_clone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentServiceImplementation implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;


    private UserService userService;


    private ReactionService reactionService;

    @Autowired
    public CommentServiceImplementation(ReactionService reactionService, UserService userService){
        this.reactionService = reactionService;
        this.userService = userService;
    }

    @Override
    public Comment findOneById(Integer id) {

        Optional<Comment> commentOpt = commentRepository.findById(id);

        if (commentOpt.isPresent()) {
            return commentOpt.get();
        }

        return null;

    }

    @Override
    public List<Comment> findAll() {
        return commentRepository.findAll();
    }

    @Override
    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public Comment createComment(CommentDTO commentDTO) {

        Comment comment = new Comment();
        comment.setText(commentDTO.getText());
        comment.setIsDeleted(false);
        comment.setCreationDate(new Date());
        comment.setPost(postRepository.getById(commentDTO.getPostId()));
        comment.setUser(userRepository.getById(commentDTO.getUserId()));

        commentRepository.save(comment);

        User user = getAuthUser();
        if (user != null) {
            Reaction reaction = new Reaction();
            reaction.setType(ReactionType.UPVOTE);
            reaction.setUser(user);
            reaction.setComment(comment);
            reactionService.createReaction(new ReactionDTO(reaction));
        }

        return comment;
    }

    public User getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // Check if the user is authenticated
        if (authentication != null && authentication.isAuthenticated()
                &&  authentication.getPrincipal() instanceof UserDetails) {
            // Get the username or other user details
            UserDetails user = (UserDetails) authentication.getPrincipal();
            User userDetails = userService.findOne(user.getUsername());
            return userDetails;
        } else {
            return null;
        }
    }

    public void setReactionByAuthUser(List<CommentDTO> dtos) {

        User user = getAuthUser();
        if (user == null) {
            return;
        }

        List<Integer> commentIds = dtos.stream()
                .map(CommentDTO::getCommentId)
                .collect(Collectors.toList());

        List<Reaction> reactions = reactionService.findByUserIdAndCommentIdIn(user.getId(), commentIds);


        // Make a map to get the reaction
        Map<Integer, Reaction> commentIdReactionMap = reactions.stream()
                .filter(reaction -> commentIds.contains(reaction.getComment().getCommentId())) // Filter by commentIds
                .collect(Collectors.toMap(reaction -> reaction.getComment().getCommentId(), reaction -> reaction));

        for(CommentDTO commentDTO: dtos) {

            if (commentIdReactionMap.containsKey(commentDTO.getCommentId())) {
                Reaction reaction = commentIdReactionMap.get(commentDTO.getCommentId());
                commentDTO.setReactionByAuthUser(reaction.getType());

            }
        }
    }


}
