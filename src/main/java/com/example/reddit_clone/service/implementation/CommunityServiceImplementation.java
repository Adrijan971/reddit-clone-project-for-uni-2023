package com.example.reddit_clone.service.implementation;

import com.example.reddit_clone.dto.CommunityDTO;
import com.example.reddit_clone.elastic.elasticsearch.ElasticCommunityRepository;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.repository.CommunityRepository;
import com.example.reddit_clone.repository.PostRepository;
import com.example.reddit_clone.service.CommunityService;
import com.example.reddit_clone.service.FlairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CommunityServiceImplementation implements CommunityService {

    @Autowired
    private CommunityRepository communityRepository;

    @Autowired
    private ElasticCommunityRepository elasticCommunityRepository;

    @Autowired
    private FlairService flairService;

    @Override
    public Community getOne(Integer id) {

        Optional<Community> communityOpt = communityRepository.findById(id);

        if (communityOpt.isPresent()) {
            return communityOpt.get();
        }

        return null;
    }

    @Override
    public List<Community> findAll() {
        return communityRepository.findAll();
    }

    @Override
    public Community save(Community community) {


        Community saved = communityRepository.save(community);
        Community forElastic = new Community();
        forElastic.setCommunityId(saved.getCommunityId());
        forElastic.setDescriptionPDF(saved.getDescriptionPDF());
        forElastic.setName(saved.getName());
        forElastic.setDescription(saved.getDescription());
        elasticCommunityRepository.save(forElastic);

        return saved;
    }

    @Override
    public Community createCommunity(CommunityDTO communityDto) {

        Community community = new Community();
        community.setName(communityDto.getName());
        community.setDescription(communityDto.getDescription());
        community.setCreationDate(new Date());
        community.setIsSuspended("false");
        community.setDescriptionPDF(communityDto.getPDFtext());

        if (communityDto.getFlairs() != null){
            // Get Flairs from Flair IDs
            Set<Flair> flairs = getFlairsFromIds(communityDto.getFlairs());

            community.setFlairs(flairs);

            for (Flair flair : flairs){
                flair.getCommunities().add(community);
            }
        }
        Community saved = communityRepository.save(community);

        Community forElastic = new Community();
        forElastic.setCommunityId(saved.getCommunityId());
        forElastic.setDescriptionPDF(saved.getDescriptionPDF());
        forElastic.setName(saved.getName());
        forElastic.setDescription(saved.getDescription());
        elasticCommunityRepository.save(forElastic);


        return community;
    }


    public Set<Flair> getFlairsFromIds(Set<Integer> ids){

        Set<Flair> flairList = new HashSet<>();

        for (Integer i : ids){
            Flair flair = flairService.findOneById(i);

            // if at least one flairId is wrong, return null
            if(flair == null){
                return null;
            }
            flairList.add(flair);
        }

        return flairList;
    }

    @Override
    public List<Community> searchByName(String searchText) {
        List<Community> elasticCommunities = elasticCommunityRepository.findAllByName(searchText);
        List<Integer> communityIds = elasticCommunities.stream()
                .map(Community::getCommunityId)
                .collect(Collectors.toList());

        List<Community> jpaCommunities = communityRepository.findAllById(communityIds);

        return jpaCommunities;
    }

    @Override
    public List<Community> searchByDescription(String searchText) {
        List<Community> elasticCommunities = elasticCommunityRepository.findAllByDescription(searchText);
        List<Integer> communityIds = elasticCommunities.stream()
                .map(Community::getCommunityId)
                .collect(Collectors.toList());

        List<Community> jpaCommunities = communityRepository.findAllById(communityIds);
        return jpaCommunities;
    }

    @Override
    public List<Community> searchByPDFDescription(String searchText) {
        List<Community> elasticCommunities = elasticCommunityRepository.findAllByDescriptionPDF(searchText);
        List<Integer> communityIds = elasticCommunities.stream()
                .map(Community::getCommunityId)
                .collect(Collectors.toList());

        List<Community> jpaCommunities = communityRepository.findAllById(communityIds);
        return jpaCommunities;
    }

    @Override
    public List<Community> searchByPostCount(int minPosts , int maxPosts) {
        List<Community> jpaCommunities = communityRepository.findCommunitiesWithPostsCountInRange(minPosts, maxPosts);

        return jpaCommunities;
    }


}
