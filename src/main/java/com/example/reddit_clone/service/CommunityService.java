package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.CommunityDTO;
import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Post;

import java.util.List;

public interface CommunityService {

    Community getOne(Integer id);
    List<Community> findAll();
    Community save(Community community);
    Community createCommunity(CommunityDTO communityDto);

    List<Community> searchByName(String searchText);

    List<Community> searchByDescription(String searchText);

    List<Community> searchByPDFDescription(String searchText);

    List<Community> searchByPostCount (int minPosts , int maxPosts);


}
