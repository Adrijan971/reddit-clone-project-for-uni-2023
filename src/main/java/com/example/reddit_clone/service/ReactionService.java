package com.example.reddit_clone.service;

import com.example.reddit_clone.dto.ReactionDTO;
import com.example.reddit_clone.model.Reaction;

import java.util.List;

public interface ReactionService {

    Reaction findOneById(Integer id);
    List<Reaction> findAll();
    List<Reaction> findByUserIdAndPostIdIn(Integer userId, List<Integer> postIds);

    List<Reaction> findByUserIdAndCommentIdIn(Integer userId, List<Integer> CommentIds);

    List<Reaction> findByPostId(Integer id);

    Reaction save(Reaction reaction);

    Reaction createReaction(ReactionDTO reactionDTO);

}
