package com.example.reddit_clone.controller;


import com.example.reddit_clone.config.SwaggerConfig;
import com.example.reddit_clone.dto.CommentDTO;
import com.example.reddit_clone.dto.CommunityDTO;
import com.example.reddit_clone.dto.UserDTO;
import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.service.CommentService;
import com.example.reddit_clone.service.CommunityService;
import com.example.reddit_clone.service.PostService;
import com.example.reddit_clone.service.UserService;
import io.swagger.annotations.Api;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("api/comments")
@Api(tags = {SwaggerConfig.COMMENTS_TAG})
public class CommentController {

    private final PostService postService;

    CommentService commentService;

    @Autowired
    UserService userService;

    @Autowired
    public CommentController(PostService postService, CommentService commentService){
        this.postService = postService;
        this.commentService = commentService;
    }

    Logger logger = Logger.getLogger(CommentController.class.getName());

    @GetMapping
    public ResponseEntity<List<CommentDTO>> findAll(){
        List<Comment> comments = commentService.findAll();

        List<CommentDTO> commentDTOS = new ArrayList<>();

        for (Comment comment : comments){
            commentDTOS.add(new CommentDTO(comment));
        }

        commentService.setReactionByAuthUser(commentDTOS);

        // return also whole object user
        for (CommentDTO commentDTO : commentDTOS){
            setUsersOnCommentDTO(commentDTO);
        }

        return new ResponseEntity<>(commentDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommentDTO> getOne(@PathVariable("id") int id){
        Comment comment = commentService.findOneById(id);

        if (comment == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        CommentDTO commentDTO = new CommentDTO(comment);

        List<CommentDTO> commentDTOS = new ArrayList<CommentDTO>();
        commentDTOS.add(commentDTO);
        commentService.setReactionByAuthUser(commentDTOS);

        setUsersOnCommentDTO(commentDTO);

        return new ResponseEntity<>(commentDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<CommentDTO> createComment(@RequestBody CommentDTO newComment){

        Comment createdComment = commentService.createComment(newComment);

        if(createdComment == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        CommentDTO commentDTO = new CommentDTO(createdComment);

        logger.info("COMMENT " + commentDTO.getCommentId() + " CREATED!");

        List<CommentDTO> commentDTOS = new ArrayList<CommentDTO>();
        commentDTOS.add(commentDTO);
        commentService.setReactionByAuthUser(commentDTOS);

        setUsersOnCommentDTO(commentDTO);

        return new ResponseEntity<>(commentDTO, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockComment/{id}")
    public ResponseEntity<CommentDTO> blockComment(@PathVariable Integer id) {

        Comment comment = commentService.findOneById(id);

        if (comment == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        comment.setIsDeleted(true);

        commentService.save(comment);

        logger.info("COMMENT " + comment.getCommentId() + " BLOCKED!");

        setUsersOnCommentDTO(new CommentDTO(comment));

        return new ResponseEntity<>(new CommentDTO(comment), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<CommentDTO> updateComment(@RequestBody CommentDTO commentDTO, @PathVariable("id") Integer id) {

        Comment comment = commentService.findOneById(id);

        if (comment == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (commentDTO.getText() != null){
            comment.setText(commentDTO.getText());
        }
        if (commentDTO.getPostId() != 0){
            comment.setPost(postService.getOne(commentDTO.getPostId()));
        }
        if (commentDTO.getUserId() != 0){
            comment.setUser(userService.findOneById(commentDTO.getUserId()));
        }

        commentService.save(comment);

        logger.info("COMMENT " + commentDTO.getCommentId() + " UPDATED!");

        setUsersOnCommentDTO(new CommentDTO(comment));

        return new ResponseEntity<>(new CommentDTO(comment), HttpStatus.OK);
    }


    public void setUsersOnCommentDTO(CommentDTO commentDTO){

        User user = userService.findOneById(commentDTO.getUserId());
        if (user != null){
            commentDTO.setUser(new UserDTO(user));
        }
    }



}
