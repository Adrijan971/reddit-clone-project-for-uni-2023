package com.example.reddit_clone.controller;

import com.example.reddit_clone.config.SwaggerConfig;
import com.example.reddit_clone.dto.BannedDTO;
import com.example.reddit_clone.model.Banned;
import com.example.reddit_clone.service.BannedService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("api/banned")
@Api(tags = {SwaggerConfig.BANNED})
public class BannedController {

    @Autowired
    BannedService bannedService;

    Logger logger = Logger.getLogger(CommentController.class.getName());

    @GetMapping
    public ResponseEntity<List<BannedDTO>> findAll(){
        List<Banned> banned = bannedService.findAll();

        List<BannedDTO> bannedDTOS = new ArrayList<>();

        for (Banned b : banned){
            bannedDTOS.add(new BannedDTO(b));
        }

        return new ResponseEntity<>(bannedDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BannedDTO> getOne(@PathVariable("id") int id){

        Banned banned = bannedService.findOneById(id);

        if (banned == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        BannedDTO bannedDTO = new BannedDTO(banned);

        return new ResponseEntity<>(bannedDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<BannedDTO> createBan(@RequestBody BannedDTO newBan){

        Banned createdBan = bannedService.createBanned(newBan);

        if(createdBan == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        logger.info("BAN " + newBan.getBannedId() + " CREATED!");

        return new ResponseEntity<>(new BannedDTO(createdBan), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockBan/{id}")
    public ResponseEntity<BannedDTO> blockBan(@PathVariable Integer id) {

        Banned banned = bannedService.findOneById(id);

        if (banned == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        banned.setActive(false);

        bannedService.save(banned);

        logger.info("BANNED " + banned.getBannedId() + " BLOCKED!");

        return new ResponseEntity<>(new BannedDTO(banned), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<BannedDTO> updateBan(@RequestBody BannedDTO bannedDTO, @PathVariable("id") Integer id) {

        Banned banned = bannedService.findOneById(id);

        if (banned == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (bannedDTO.getBannedReason() != null){
            banned.setBannedReason(bannedDTO.getBannedReason());
        }

        bannedService.save(banned);

        logger.info("BANNED " + banned.getBannedId() + " UPDATED!");

        return new ResponseEntity<>(new BannedDTO(banned), HttpStatus.OK);
    }


}
