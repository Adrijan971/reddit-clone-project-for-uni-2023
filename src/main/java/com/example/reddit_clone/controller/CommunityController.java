package com.example.reddit_clone.controller;

import com.example.reddit_clone.config.SwaggerConfig;
import com.example.reddit_clone.dto.CommunityDTO;
import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.dto.SearchRequestDTO;
import com.example.reddit_clone.elastic.filters.PredprocessorFilter;
import com.example.reddit_clone.model.Banned;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.repository.BannedRepository;
import com.example.reddit_clone.service.CommunityService;
import com.example.reddit_clone.service.PostService;
import com.example.reddit_clone.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("api/communities")
@Api(tags = {SwaggerConfig.COMMUNITY_TAG})

public class CommunityController {

    @Autowired
    PostService postService;

    @Autowired
    BannedRepository bannedRepository;

    @Autowired
    UserService userService;

    @Autowired
    CommunityService communityService;

    Logger logger = Logger.getLogger(CommunityController.class.getName());

    @GetMapping
    public ResponseEntity<List<CommunityDTO>> findAll(){
        List<Community> communities = communityService.findAll();

        List<CommunityDTO> communityDTOS = new ArrayList<>();

        for (Community community : communities){
            community.setFlairs(new HashSet<>());
            communityDTOS.add(new CommunityDTO(community));
        }
        return new ResponseEntity<>(communityDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommunityDTO> getOne(@PathVariable("id") int id){
        Community community = communityService.getOne(id);

        if (community == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        community.setFlairs(new HashSet<>());

        CommunityDTO communityDTO = new CommunityDTO(community);
        List<Banned> allBans = bannedRepository.findAll();
        for(Banned ban: allBans) {
            if (ban.getCommunity().getCommunityId() == communityDTO.getCommunityId()) {
                communityDTO.setIsSuspended("true");

            }
        }


        return new ResponseEntity<>(communityDTO, HttpStatus.OK);
    }

    @PostMapping(path = "/create", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ApiOperation(value = "Create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommunityDTO> createCommunity(@ModelAttribute CommunityDTO newCommunity) throws IOException {

        if (newCommunity.getFile() != null && newCommunity.getFile().getContentType().equals("application/pdf")) {
            //  ako sam poslao pdf fajl, setujem ga u taj isti dto koji je stigao
            InputStream is = newCommunity.getFile().getInputStream();
            PDDocument document = PDDocument.load(is);
            PDFTextStripper pdfStripper = new PDFTextStripper();
            newCommunity.setPDFtext(pdfStripper.getText(document));

            is.close();
            document.close();
        }

        Community createdCommunity = communityService.createCommunity(newCommunity);

        if(createdCommunity == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        CommunityDTO communityDTO = new CommunityDTO(createdCommunity);

        logger.info("COMMUNITY " + communityDTO.getName() + " CREATED!");

        return new ResponseEntity<>(communityDTO, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockCommunity/{id}")
    public ResponseEntity<CommunityDTO> blockCommunity(@PathVariable Integer id) {

        Community community = communityService.getOne(id);

        if (community == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        community.setIsSuspended("true");

        communityService.save(community);

        logger.info("COMMUNITY " + community.getName() + " BLOCKED!");

        return new ResponseEntity<>(new CommunityDTO(community), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<CommunityDTO> updateCommunity(@RequestBody CommunityDTO communityDTO, @PathVariable("id") Integer id) {
        Community community = communityService.getOne(id);

        if (community == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (communityDTO.getName() != null){
            community.setName(communityDTO.getName());
        }
        if (communityDTO.getDescription() != null){
            community.setDescription(communityDTO.getDescription());
        }

        communityService.save(community);

        logger.info("COMMUNITY " + communityDTO.getName() + " UPDATED!");

        return new ResponseEntity<>(new CommunityDTO(community), HttpStatus.OK);
    }

    @PostMapping(value = "/search", consumes = "application/json")
    public ResponseEntity<List<CommunityDTO>> search(@RequestBody SearchRequestDTO searchDTO) {
        List<Community> communities;
        String searchText = searchDTO.getSearchText();

        int minCount = searchDTO.getMinCount();
        int maxCount = searchDTO.getMaxCount();

        searchText = PredprocessorFilter.cyrillicToLatin(searchText).toLowerCase();

        if(searchDTO.isSearchByDescription()) {
            communities = communityService.searchByDescription(searchText);
        }
        if(searchDTO.isSearchByPostCount()) {
            communities = communityService.searchByPostCount(minCount, maxCount);
        } else if (searchDTO.isSearchByName()) {
            communities = communityService.searchByName(searchText);
        }

        else {
            communities = communityService.searchByPDFDescription(searchText);
        }

        List<CommunityDTO> communityDTOS = new ArrayList<>();

        for (Community community : communities){
            communityDTOS.add(new CommunityDTO(community));
        }
        return new ResponseEntity<>(communityDTOS, HttpStatus.OK);
    }

}
