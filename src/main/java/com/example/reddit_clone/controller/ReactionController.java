package com.example.reddit_clone.controller;


import com.example.reddit_clone.config.SwaggerConfig;
import com.example.reddit_clone.dto.ReactionDTO;
import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.service.ReactionService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/reactions")
@Api(tags = {SwaggerConfig.REACTION_TAG})

public class ReactionController {

    @Autowired
    ReactionService reactionService;

    Logger logger = Logger.getLogger(ReactionController.class.getName());


    @GetMapping
    public ResponseEntity<List<ReactionDTO>> findAll(){

        List<Reaction> reactions = reactionService.findAll();

        List<ReactionDTO> reactionDTOS = new ArrayList<>();

        for (Reaction reaction : reactions){
            reactionDTOS.add(new ReactionDTO(reaction));
        }
        return new ResponseEntity<>(reactionDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReactionDTO> getOne(@PathVariable("id") int id){
        Reaction reaction = reactionService.findOneById(id);

        if (reaction == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        ReactionDTO reactionDTO = new ReactionDTO(reaction);

        return new ResponseEntity<>(reactionDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<ReactionDTO> createReaction(@RequestBody ReactionDTO newReaction){

        if (newReaction.getPostId() != 0 && newReaction.getCommentId() != 0){
            logger.info("Cannot provide both: commentId and postId! ");

            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST);
        }

        Reaction createdReaction = reactionService.createReaction(newReaction);

        if(createdReaction == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        logger.info("REACTION " + newReaction.getReactionId() + " CREATED!");

        return new ResponseEntity<>(newReaction, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockReaction/{id}")
    public ResponseEntity<ReactionDTO> blockReaction(@PathVariable Integer id) {

        Reaction reaction = reactionService.findOneById(id);

        if (reaction == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        reaction.setActive(false);

        reactionService.save(reaction);

        logger.info("REACTION " + reaction.getReactionId() + " BLOCKED!");

        return new ResponseEntity<>(new ReactionDTO(reaction), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<ReactionDTO> updateReaction(@RequestBody ReactionDTO reactionDTO, @PathVariable("id") Integer id) {

        Reaction reaction = reactionService.findOneById(id);

        if (reaction == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (reactionDTO.getType() != null){
            reaction.setType(reactionDTO.getType());
        }

        reactionService.save(reaction);

        logger.info("REACTION " + reactionDTO.getReactionId() + " UPDATED!");

        return new ResponseEntity<>(new ReactionDTO(reaction), HttpStatus.OK);
    }
}


