package com.example.reddit_clone.controller;


import com.example.reddit_clone.config.SwaggerConfig;
import com.example.reddit_clone.dto.PostDTO;
import com.example.reddit_clone.dto.SearchRequestDTO;
import com.example.reddit_clone.elastic.filters.PredprocessorFilter;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.service.FlairService;
import com.example.reddit_clone.service.PostService;
import com.example.reddit_clone.service.ReactionService;
import com.example.reddit_clone.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/posts")
@Api(tags = {SwaggerConfig.POST_TAG})
public class PostController {

    @Autowired
    PostService postService;

    @Autowired
    UserService userService;

    @Autowired
    FlairService flairService;

    @Autowired
    ReactionService reactionService;

    Logger logger = Logger.getLogger(PostController.class.getName());

    @GetMapping
    public ResponseEntity<List<PostDTO>> findAll(){
        List<Post> posts = postService.findAll();

        List<PostDTO> postDTOS = new ArrayList<>();

        for (Post post : posts){
            postDTOS.add(new PostDTO(post));
        }

        //setKarmaForPostDto
        for (PostDTO postDto : postDTOS){
            postService.setKarmaForPostDto(postDto);
        }
        postService.setReactionByAuthUser(postDTOS);

        return new ResponseEntity<>(postDTOS, HttpStatus.OK);
    };

    @GetMapping("/{id}")
    public ResponseEntity<PostDTO> getOne(@PathVariable("id") int id){
        Post post = postService.getOne(id);

        if (post == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        PostDTO postDTO = new PostDTO(post);
        List<PostDTO> dtos = new ArrayList<>();
        dtos.add(postDTO);
        postService.setReactionByAuthUser(dtos);


        return new ResponseEntity<>(postDTO, HttpStatus.OK);
    }


    @PostMapping(path = "/create", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ApiOperation(value = "Create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PostDTO> createPost(@ModelAttribute PostDTO newPost) throws IOException {

        if (newPost.getFile() != null && newPost.getFile().getContentType().equals("application/pdf")) {

            //  ako sam poslao pdf fajl, setujem ga u taj isti dto koji je stigao
            InputStream is = newPost.getFile().getInputStream();
            PDDocument document = PDDocument.load(is);
            PDFTextStripper pdfStripper = new PDFTextStripper();
            newPost.setPDFtext(pdfStripper.getText(document));

            is.close();
            document.close();
        }

        Post createdPost = postService.createPost(newPost);

        if(createdPost == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        PostDTO postDTO = new PostDTO(createdPost);
        List<PostDTO> dtos = new ArrayList<>();
        dtos.add(postDTO);
        postService.setReactionByAuthUser(dtos);

        logger.info("POST " + postDTO.getTitle() + " CREATED!");

        return new ResponseEntity<>(postDTO, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockPost/{id}")
    public ResponseEntity<PostDTO> blockPost(@PathVariable Integer id) {

        Post post = postService.getOne(id);

        if (post == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        post.setActive(false);

        postService.save(post);

        logger.info("POST " + post.getTitle() + " BLOCKED!");

        return new ResponseEntity<>(new PostDTO(post), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<PostDTO> updatePost(@RequestBody PostDTO postDTO, @PathVariable("id") Integer id) {
        Post post = postService.getOne(id);

        if (post == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (postDTO.getTitle() != null){
            post.setTitle(postDTO.getTitle());
        }
        if (postDTO.getText() != null){
            post.setText(postDTO.getText());
        }
        if (postDTO.getUser() != 0){
            post.setUser(userService.findOneById(postDTO.getUser()));
        }
        if (postDTO.getFlair() != 0){
            post.setFlair(flairService.findOneById(postDTO.getFlair()));
        }

        postService.save(post);

        logger.info("POST " + postDTO.getTitle() + " UPDATED!");

        return new ResponseEntity<>(new PostDTO(post), HttpStatus.OK);
    }


    @PostMapping(value = "/search", consumes = "application/json")
    public ResponseEntity<List<PostDTO>> search(@RequestBody SearchRequestDTO searchDTO) {

        List<Post> posts = new ArrayList<Post>();
        List<PostDTO> postsDTOlist = new ArrayList<PostDTO>();

        String searchText = searchDTO.getSearchText();

        int minCount = searchDTO.getMinCount();
        int maxCount = searchDTO.getMaxCount();

        searchText = PredprocessorFilter.cyrillicToLatin(searchText).toLowerCase();
        if(searchDTO.isSearchByDescription()) {
            posts = postService.searchByDescription(searchText);
            for (Post post : posts){
                postsDTOlist.add(new PostDTO(post));
            }
        } else if (searchDTO.isSearchByTitle()) {
            posts = postService.searchByTitle(searchText);
            for (Post post : posts){
                postsDTOlist.add(new PostDTO(post));
            }
        } else if (searchDTO.isSearchByKarmaRange()) {
            postsDTOlist = postService.searchByKarmaRange(minCount, maxCount);
        }
        else if (searchDTO.isSearchByPDFDescription()) {
            posts = postService.searchByPDFDescription(searchText);
            for (Post post : posts){
                postsDTOlist.add(new PostDTO(post));
            }
        }

        postService.setReactionByAuthUser(postsDTOlist);

        return new ResponseEntity<>(postsDTOlist, HttpStatus.OK);

    }


}
