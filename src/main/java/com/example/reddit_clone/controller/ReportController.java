package com.example.reddit_clone.controller;

import com.example.reddit_clone.config.SwaggerConfig;
import com.example.reddit_clone.dto.ReactionDTO;
import com.example.reddit_clone.dto.ReportDTO;
import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.model.Report;
import com.example.reddit_clone.service.ReactionService;
import com.example.reddit_clone.service.ReportService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/reports")
@Api(tags = {SwaggerConfig.REPORT_TAG})

public class ReportController {

    @Autowired
    ReportService reportService;

    Logger logger = Logger.getLogger(ReportController.class.getName());

    @GetMapping
    public ResponseEntity<List<ReportDTO>> findAll(){

        List<Report> reports = reportService.findAll();

        List<ReportDTO> reportDTOS = new ArrayList<>();

        for (Report report : reports){
            reportDTOS.add(new ReportDTO(report));
        }
        return new ResponseEntity<>(reportDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReportDTO> getOne(@PathVariable("id") int id){
        Report report = reportService.findOneById(id);

        if (report == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        ReportDTO reportDTO = new ReportDTO(report);

        return new ResponseEntity<>(reportDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<ReportDTO> createReport(@RequestBody ReportDTO newReport){

        if (newReport.getPostId() != 0 && newReport.getCommentId() != 0){
            logger.info("Cannot provide both: commentId and postId! ");

            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST);
        }

        Report createdReport = reportService.createReport(newReport);

        if(createdReport == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        logger.info("REPORT " + newReport.getReportId() + " CREATED!");

        return new ResponseEntity<>(new ReportDTO(createdReport), HttpStatus.CREATED);
    }

    // this controller is not for removing the report just to accept it or not
    @PutMapping(value = "/accept/{id}")
    public ResponseEntity<ReportDTO> accept(@PathVariable Integer id) {

        Report report = reportService.findOneById(id);

        if (report == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        report.setAccepted(true);

        reportService.save(report);

        logger.info("REACTION " + report.getReportId() + " ACCEPTED!");

        return new ResponseEntity<>(new ReportDTO(report), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<ReportDTO> updateReport(@RequestBody ReportDTO reportDTO, @PathVariable("id") Integer id) {

        Report report = reportService.findOneById(id);

        if (report == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (reportDTO.getReason() != null){
            report.setReason(reportDTO.getReason());
        }

        reportService.save(report);

        logger.info("REPORT " + report.getReportId() + " UPDATED!");

        return new ResponseEntity<>(new ReportDTO(report), HttpStatus.OK);
    }

}
