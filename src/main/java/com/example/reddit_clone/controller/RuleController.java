package com.example.reddit_clone.controller;

import com.example.reddit_clone.dto.FlairDTO;
import com.example.reddit_clone.dto.ReportDTO;
import com.example.reddit_clone.dto.RuleDTO;
import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.Report;
import com.example.reddit_clone.model.Rule;
import com.example.reddit_clone.service.CommunityService;
import com.example.reddit_clone.service.ReportService;
import com.example.reddit_clone.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/rules")
public class RuleController {

    @Autowired
    RuleService ruleService;

    @Autowired
    CommunityService communityService;

    Logger logger = Logger.getLogger(RuleController.class.getName());

    @GetMapping
    public ResponseEntity<List<RuleDTO>> findAll(){

        List<Rule> rules = ruleService.findAll();

        List<RuleDTO> ruleDTOS = new ArrayList<>();

        for (Rule rule : rules){
            ruleDTOS.add(new RuleDTO(rule));
        }
        return new ResponseEntity<>(ruleDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RuleDTO> getOne(@PathVariable("id") int id){

        Rule rule = ruleService.findOneById(id);

        if (rule == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        RuleDTO ruleDTO = new RuleDTO(rule);

        return new ResponseEntity<>(ruleDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<RuleDTO> createRule(@RequestBody RuleDTO newRule){

        Rule createdRule = ruleService.createRule(newRule);

        if(createdRule == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        logger.info("RULE " + newRule.getRuleId() + " CREATED!");

        return new ResponseEntity<>(new RuleDTO(createdRule), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockRule/{id}")
    public ResponseEntity<RuleDTO> blockRule(@PathVariable Integer id) {

        Rule rule = ruleService.findOneById(id);

        if (rule == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        rule.setActive(false);

        ruleService.save(rule);

        logger.info("RULE " + rule.getRuleId() + " BLOCKED!");

        return new ResponseEntity<>(new RuleDTO(rule), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<RuleDTO> updateRule(@RequestBody RuleDTO ruleDTO, @PathVariable("id") Integer id) {

        Rule rule = ruleService.findOneById(id);

        if (rule == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (ruleDTO.getDescription() != null){
            rule.setDescription(ruleDTO.getDescription());
        }
        if (ruleDTO.getCommunity() != 0){
            System.out.println("NOT 000000000000000000000");
            rule.setCommunity(communityService.getOne(ruleDTO.getCommunity()));
        }

        ruleService.save(rule);

        logger.info("RULE " + rule.getRuleId() + " UPDATED!");

        return new ResponseEntity<>(new RuleDTO(rule), HttpStatus.OK);
    }

}
