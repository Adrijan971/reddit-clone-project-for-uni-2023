package com.example.reddit_clone.controller;

import com.example.reddit_clone.dto.FlairDTO;
import com.example.reddit_clone.dto.ReportDTO;
import com.example.reddit_clone.dto.UserDTO;
import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.Report;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.service.FlairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("api/flairs")
public class FlairController {

    @Autowired
    FlairService flairService;

    Logger logger = Logger.getLogger(FlairController.class.getName());

    @GetMapping
    public ResponseEntity<List<FlairDTO>> findAll(){

        List<Flair> flairs = flairService.findAll();

        List<FlairDTO> flairDTOS = new ArrayList<>();

        for (Flair flair : flairs){
            flairDTOS.add(new FlairDTO(flair));
        }
        return new ResponseEntity<>(flairDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FlairDTO> getOne(@PathVariable("id") int id){

        Flair flair = flairService.findOneById(id);

        if (flair == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        FlairDTO flairDTO = new FlairDTO(flair);

        return new ResponseEntity<>(flairDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<FlairDTO> createFlair(@RequestBody FlairDTO newFlair){

        Flair createdFlair = flairService.createFlair(newFlair);

        if(createdFlair == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        logger.info("FLAIR " + newFlair.getFlairId() + " CREATED!");

        return new ResponseEntity<>(new FlairDTO(createdFlair), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockFlair/{id}")
    public ResponseEntity<FlairDTO> blockFlair(@PathVariable Integer id) {

        Flair flair = flairService.findOneById(id);

        if (flair == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        flair.setActive(false);

        flairService.save(flair);

        logger.info("FLAIR " + flair.getName() + " BLOCKED!");

        return new ResponseEntity<>(new FlairDTO(flair), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<FlairDTO> updateFlair(@RequestBody FlairDTO flairDTO, @PathVariable("id") Integer id) {

        Flair flair = flairService.findOneById(id);

        if (flair == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (flairDTO.getName() != null){
            flair.setName(flairDTO.getName());
        }

        flairService.save(flair);

        logger.info("FLAIR " + flair.getFlairId() + " UPDATED!");

        return new ResponseEntity<>(new FlairDTO(flair), HttpStatus.OK);
    }

}
