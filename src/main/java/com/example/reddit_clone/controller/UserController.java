package com.example.reddit_clone.controller;

import com.example.reddit_clone.config.SwaggerConfig;
import com.example.reddit_clone.dto.ReactionDTO;
import com.example.reddit_clone.dto.UserDTO;
import com.example.reddit_clone.dto.UserLoginDTO;
import com.example.reddit_clone.dto.UserLoginResponseDTO;
import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.model.enums.Roles;
import com.example.reddit_clone.security.TokenUtils;
import com.example.reddit_clone.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("api/users")
@Api(tags = {SwaggerConfig.USER_TAG})

public class UserController {

    @Autowired
    UserService userService;

    Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    TokenUtils tokenUtils;

    @Autowired
    AuthenticationManager authenticationManager;

    @GetMapping
    public ResponseEntity<List<UserDTO>> findAll(){
        List<User> users = userService.findAll();

        List<UserDTO> userDTOS = new ArrayList<>();

        for (User user : users){
            userDTOS.add(new UserDTO(user));
        }
        // set karma and comments on dto
        for (UserDTO userDTO : userDTOS){
            userService.setKarmaForUserDto(userDTO);
            userService.setCommentListForUserDto(userDTO);
        }

        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    };


    @GetMapping("/byId/{id}")
    public ResponseEntity<UserDTO> getOneById(@PathVariable("id") int id){
        User user = userService.findOneById(id);

        if (user == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        UserDTO userDTO = new UserDTO(user);
        // set karma and comments on dto
        userService.setKarmaForUserDto(userDTO);
        userService.setCommentListForUserDto(userDTO);

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<UserDTO> getOneByUsername(@PathVariable("username") String username){
        Optional<User> user = Optional.ofNullable(userService.findOne(username));
        if(!user.isPresent()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        UserDTO userDTO = new UserDTO(user.get());
        // set karma on dto
        userService.setKarmaForUserDto(userDTO);
        userService.setCommentListForUserDto(userDTO);

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO newUser){

        User createdUser = userService.createUser(newUser);

        if(createdUser == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        UserDTO userDTO = new UserDTO(createdUser);

        logger.info("USER " + userDTO.getUsername() + " CREATED!");

        return new ResponseEntity<>(userDTO, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/blockUser/{id}")
    public ResponseEntity<UserDTO> blockUser(@PathVariable Integer id) {

        User user = userService.findOneById(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        user.setActive(false);

        user = userService.save(user);

        logger.info("USER " + user.getUsername() + " BLOCKED!");

        return new ResponseEntity<>(new UserDTO(user), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{username}", consumes = "application/json")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO, @PathVariable("username") String username) {
        User user = userService.findOne(username);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (userDTO.getEmail() != null){
            user.setEmail(userDTO.getEmail());
        }
        if (userDTO.getDisplayName() != null){
            user.setDisplayName(userDTO.getDisplayName());
        }
        if (userDTO.getDescription() != null){
            user.setDescription(userDTO.getDescription());
        }
        if (userDTO.getAvatar() != null){
            user.setAvatar(userDTO.getAvatar());
        }

        user = userService.save(user);

        logger.info("USER " + userDTO.getUsername() + " UPDATED!");

        return new ResponseEntity<>(new UserDTO(user), HttpStatus.OK);
    }

        @PostMapping("/login")
        public ResponseEntity<UserLoginResponseDTO> createAuthenticationToken(
            @RequestBody UserLoginDTO authenticationRequest, HttpServletResponse response) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getUsername(), authenticationRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails user = (UserDetails) authentication.getPrincipal();
        String jwt = tokenUtils.generateToken(user);
        User userDetails = userService.findOne(user.getUsername());
        UserDTO userDTO = new UserDTO(userDetails);

        // set karma and comments on dto
        userService.setKarmaForUserDto(userDTO);
        userService.setCommentListForUserDto(userDTO);

        UserLoginResponseDTO dto = new UserLoginResponseDTO();
        dto.setToken(jwt);
        dto.setUserDTO(userDTO);

        return ResponseEntity.ok(dto);
    }
}
