package com.example.reddit_clone;

import com.example.reddit_clone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebMvc
@EnableSwagger2
@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.example.reddit_clone.repository")
@EnableElasticsearchRepositories(basePackages = "com.example.reddit_clone.elastic")
public class RedditCloneApplication {



	public static void main(String[] args) {
		SpringApplication.run(RedditCloneApplication.class, args);
		System.out.println("Hello, World!");

	}

}
