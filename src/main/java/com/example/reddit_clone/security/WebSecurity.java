package com.example.reddit_clone.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.reddit_clone.model.User;
import com.example.reddit_clone.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Component
public class WebSecurity {


    private UserService userService;

    @Autowired
    public WebSecurity(UserService us) {
        this.userService = us;
    }

    public boolean checkId(Authentication authentication, HttpServletRequest request, int id) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userService.findOne(userDetails.getUsername());
        if(id == user.getId()) {
            return true;
        }
        return false;
    }
}
