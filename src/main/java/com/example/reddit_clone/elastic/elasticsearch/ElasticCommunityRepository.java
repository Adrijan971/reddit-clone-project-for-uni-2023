package com.example.reddit_clone.elastic.elasticsearch;

import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Post;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface ElasticCommunityRepository extends ElasticsearchRepository<Community, String> {

    //@Query("{\"match\": {\"title\": {\"query\": \"?0\",\"fuzziness\": \"AUTO\"}}}")
    List<Community> findAllByName(String title);

    List<Community> findAllByDescription(String text);

    List<Community> findAllByDescriptionPDF(String pdfText);

    List<Community> findAll();
}
