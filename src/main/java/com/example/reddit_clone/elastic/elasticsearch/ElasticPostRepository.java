package com.example.reddit_clone.elastic.elasticsearch;

import com.example.reddit_clone.model.Post;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface ElasticPostRepository  extends ElasticsearchRepository<Post, String>  {
        //@Query("{\"match\": {\"title\": {\"query\": \"?0\",\"fuzziness\": \"AUTO\"}}}")
        List<Post> findAllByTitle(String title);

        List<Post> findAllByText(String text);

        List<Post> findAllByDescriptionPDF(String pdfText);


        List<Post> findAll();
}
