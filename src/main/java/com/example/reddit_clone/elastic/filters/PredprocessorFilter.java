package com.example.reddit_clone.elastic.filters;
import com.ibm.icu.text.Transliterator;
public class PredprocessorFilter {

    private static final Transliterator CYRILLIC_TO_LATIN_TRANSLITERATOR =
            Transliterator.getInstance("Cyrillic-Latin");

    private static final Transliterator LATIN_TO_CYRILLIC_TRANSLITERATOR =
            Transliterator.getInstance("Latin-Cyrillic");

    public static void main(String[] args) {
        String cyrillicText = "тест хаос";
        int кек = 0;
        String latinText = cyrillicToLatin(cyrillicText);

        System.out.println("Cyrillic: " + cyrillicText);
        System.out.println("Latin:    " + latinText);

        String cyrilic = latinToCyrillicTo(latinText);
        System.out.println("Other way Cyrillic: " + cyrilic);

    }

    public static String cyrillicToLatin(String text) {
        if (text == null){
            return "";
        }

        return CYRILLIC_TO_LATIN_TRANSLITERATOR.transliterate(text);
    }

    public static String latinToCyrillicTo(String text) {
        if (text == null){
            return "";
        }

        return LATIN_TO_CYRILLIC_TRANSLITERATOR.transliterate(text);
    }
}
