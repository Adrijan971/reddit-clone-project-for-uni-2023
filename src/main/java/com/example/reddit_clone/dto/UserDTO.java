package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.User;
import com.example.reddit_clone.model.enums.Roles;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO implements Serializable {

    private int userId;

    private String username;

    private String password;

    private String email;

    private String avatar;

    private Date registrationDate;

    private String description;

    private String displayName;

    private Roles roles;

    private Boolean active;

    private int karma;

    private List<CommentDTO> commentList;

    public UserDTO(User createdUser) {
        this.userId = createdUser.getId();
        this.username = createdUser.getUsername();
        this.password = createdUser.getPassword();
        this.email = createdUser.getEmail();
        this.avatar = createdUser.getAvatar();
        this.registrationDate = createdUser.getRegistrationDate();
        this.description = createdUser.getDescription();
        this.displayName = createdUser.getDisplayName();
        this.roles = createdUser.getRoles();
        this.active = createdUser.getActive();
    }

}
