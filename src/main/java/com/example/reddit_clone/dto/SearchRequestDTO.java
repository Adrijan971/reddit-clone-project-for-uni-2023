package com.example.reddit_clone.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SearchRequestDTO {

    private boolean searchByTitle;
    private boolean searchByDescription;
    private boolean searchByPDFDescription;
    private boolean searchByPostCount;  // community uses this
    private boolean searchByName;
    private boolean searchByKarmaRange; // post uses this

    private String searchText;
    private int minCount;
    private int maxCount;
}
