package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.Flair;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class FlairDTO implements Serializable {

    private int flairId;

    private String name;

    private Boolean active;

    public FlairDTO(Flair flair){
        this.flairId = flair.getFlairId();
        this.name = flair.getName();
        this.active = flair.getActive();
    }

}
