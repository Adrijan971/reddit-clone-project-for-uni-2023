package com.example.reddit_clone.dto;
import com.example.reddit_clone.model.enums.ReactionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.example.reddit_clone.model.Post;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PostDTO implements Serializable {

    private int postId;
    private String title;
    private String text;
    private Date creationDate;
    private String imagePath;
    private int community;
    private Boolean active;
    private int user;
    private int flair;
    private MultipartFile file;
    private String PDFtext;
    private int reactions_sum;

    private ReactionType reactionByAuthUser;


    public PostDTO(Post createdPost) {
        this.postId = createdPost.getId();
        this.title = createdPost.getTitle();
        this.text = createdPost.getText();
        this.creationDate = createdPost.getCreationDate();
        this.imagePath = createdPost.getImagePath();
        this.active = createdPost.getActive();
        this.community = createdPost.getCommunity().getCommunityId();
        if (createdPost.getUser() != null) {
            this.user = createdPost.getUser().getId();
        }
        if ( createdPost.getFlair() != null) {
            this.flair = createdPost.getFlair().getFlairId();
        }
        this.PDFtext = createdPost.getDescriptionPDF();
    }

}
