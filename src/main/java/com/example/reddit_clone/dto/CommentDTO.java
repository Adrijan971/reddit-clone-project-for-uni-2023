package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.*;
import com.example.reddit_clone.model.enums.ReactionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class CommentDTO implements Serializable {

    private int commentId;
    private String text;
    private Date creationDate;
    private Boolean isDeleted;
    private int userId;
    private int postId;
    private ReactionType reactionByAuthUser;
    private UserDTO user;

    public CommentDTO (Comment comment){

        this.commentId = comment.getCommentId();
        this.text = comment.getText();
        this.isDeleted = comment.getIsDeleted();
        this.creationDate = comment.getCreationDate();
        this.userId = comment.getUser().getId();
        this.postId = comment.getPost().getId();
    }
}
