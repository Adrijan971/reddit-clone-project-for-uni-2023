package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Flair;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class CommunityDTO implements Serializable {

    private int communityId;
    private String name;
    private String description;
    private Date creationDate;
    private String isSuspended;
    private MultipartFile file;
    private String PDFtext;
    private Set<Integer> flairs;

    public CommunityDTO (Community community){

        this.communityId = community.getCommunityId();
        this.name = community.getName();
        this.description = community.getDescription();
        this.creationDate = community.getCreationDate();
        this.isSuspended = community.getIsSuspended();
        this.PDFtext = community.getDescriptionPDF();
        if(community.getFlairs() != null){
            this.flairs = getFlairIds(community.getFlairs());
        }

    }

    public Set<Integer> getFlairIds (Set<Flair> flairList){

        Set<Integer> list = new HashSet<>();

        for (Flair f : flairList){
            list.add(f.getFlairId());
        }

        return list;
    }
}
