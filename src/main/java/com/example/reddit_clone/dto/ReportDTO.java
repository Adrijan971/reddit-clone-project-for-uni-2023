package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.model.Report;
import com.example.reddit_clone.model.User;
import com.example.reddit_clone.model.enums.ReportReason;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ReportDTO implements Serializable {

    private int reportId;

    private ReportReason reason;

    private Date creationDate;

    private int byUser;

    private int postId;

    private int commentId;

    private boolean accepted;

    public ReportDTO(Report createdReport) {

        this.reportId = createdReport.getReportId();
        this.reason = createdReport.getReason();
        this.creationDate = createdReport.getCreationDate();
        this.byUser = createdReport.getByUser().getId();
        this.accepted = createdReport.isAccepted();

        if(createdReport.getPost() != null){
            this.postId = createdReport.getPost().getId();
        }

        if(createdReport.getComment() != null){
            this.commentId = createdReport.getComment().getCommentId();
        }
    }
}
