package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.Banned;
import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class BannedDTO implements Serializable {


    private int bannedId;
    private Date creationDate;
    private User user;
    private int by;
    private int community;
    private String bannedReason;
    private Boolean active;

    public BannedDTO (Banned banned){

        this.bannedId = banned.getBannedId();
        this.creationDate = banned.getCreationDate();
        this.bannedReason = banned.getBannedReason();
        this.by = banned.getBannedId();
        this.user = banned.getBy();
        this.community = banned.getCommunity().getCommunityId();
        this.active = banned.getActive();
    }

}
