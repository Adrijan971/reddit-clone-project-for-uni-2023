package com.example.reddit_clone.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginResponseDTO implements Serializable {

    private String token;
    private UserDTO userDTO;
}
