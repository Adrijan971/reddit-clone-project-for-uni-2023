package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.Rule;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class RuleDTO implements Serializable {

    private int ruleId;

    private String description;

    private Boolean active;

    private int community;

    public RuleDTO(Rule rule){

        this.ruleId = rule.getRuleId();
        this.active = rule.getActive();
        this.description = rule.getDescription();
        if(rule.getCommunity() != null){
            this.community = rule.getCommunity().getCommunityId();
        }


    }

}
