package com.example.reddit_clone.dto;

import com.example.reddit_clone.model.Reaction;
import com.example.reddit_clone.model.enums.ReactionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ReactionDTO implements Serializable {

    private int reactionId;
    private ReactionType type;
    private Date creationDate;
    private int userId;
    private int postId;
    private int commentId;
    private Boolean active;

    public ReactionDTO (Reaction reaction) {

        this.reactionId = reaction.getReactionId();
        this.type = reaction.getType();
        this.creationDate = reaction.getCreationDate();
        this.userId = reaction.getUser().getId();
        this.active = reaction.getActive();
        if (reaction.getPost() != null){
            this.postId = reaction.getPost().getId();
        }
        if (reaction.getComment() != null){
            this.commentId = reaction.getComment().getCommentId();
        }
    }

}
