package com.example.reddit_clone.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Entity
@NoArgsConstructor
@Document(indexName = "community")
@Setting(settingPath = "/analyzers/settings.json")
public class Community {

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Getter @Setter
    @Field(type = FieldType.Integer)
    private int communityId;

    @Column(nullable = false)
    @Getter @Setter
    @Field(type = FieldType.Text)
    private String name;

    @Column(nullable = false)
    @Getter @Setter
    @Field(type = FieldType.Text)
    private String description;

    @Column(nullable = false)
    @Getter @Setter
    private Date creationDate;

    @Column(nullable = false)
    @Getter @Setter
    private String isSuspended;

    @Column(nullable = true)
    @Getter @Setter
    private String communityPdfPath;

    // it is 'text' because it has to accept a lot of data
    @Column(columnDefinition = "text", nullable = true)
    @Getter @Setter
    @Field(type = FieldType.Text)
    private String descriptionPDF;

    //not needed to define on this side also
    //@OneToMany(mappedBy = "community", fetch = FetchType.LAZY)
    //private Set<Banned> banned;


    // see what happens to this one, not yet decided
    @OneToOne
    @JoinColumn(name = "user_id")
    @Getter @Setter
    private User moderator;

    @OneToMany(mappedBy = "community", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Post> posts;

    // many to many needs to stay defined
    @JsonIgnore
    @ManyToMany(mappedBy = "communities",fetch = FetchType.LAZY)
    @Getter @Setter
    private Set<Flair> flairs;

    //not needed to define on this side also
    //@OneToMany(mappedBy = "community", fetch = FetchType.LAZY)
    //private Set<Rule> rules;

}
