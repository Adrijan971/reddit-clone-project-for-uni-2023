package com.example.reddit_clone.model.enums;

public enum ReactionType {
    UPVOTE, DOWNVOTE
}
