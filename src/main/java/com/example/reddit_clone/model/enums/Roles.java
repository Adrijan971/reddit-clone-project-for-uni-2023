package com.example.reddit_clone.model.enums;

public enum Roles {

    USER,
    ADMIN,
    MODERATOR,

}
