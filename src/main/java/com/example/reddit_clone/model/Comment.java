package com.example.reddit_clone.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int commentId;

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private Date creationDate;

    @Column(nullable = false)
    private Boolean isDeleted;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    private Post post;

//    @OneToMany(mappedBy = "comment", fetch = FetchType.LAZY)
//    private Set<Reaction> reactions;
//
//    @OneToMany(mappedBy = "comment", fetch = FetchType.LAZY)
//    private Set<Report> reports;

}
