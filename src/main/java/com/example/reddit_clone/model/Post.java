package com.example.reddit_clone.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Entity
@Document(indexName = "posts")
@Setting(settingPath = "/analyzers/settings.json")
@NoArgsConstructor
public class Post {


    // za svaki post u bazu -> ide i u elastic pod istim id-em

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Field(type = FieldType.Integer, name="jpaId")
    private int id;

    @Column(nullable = false)
    @Field(type = FieldType.Text)
    private String title;

    @Column(nullable = false)
    @Field(type = FieldType.Text)
    private String text;

    @Column(nullable = false)
    private Date creationDate;

    @Column(nullable = true)
    private String imagePath;

    @Column(nullable = true)
    private String postPDFpath;

    // it is 'text' because it has to accept a lot of data
    @Column(columnDefinition = "text", nullable = true)
    @Field(type = FieldType.Text)
    private String descriptionPDF;

    @Column(nullable = false)
    private Boolean active;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // videti kako ovo dalje sprovesti i da li da bude atribut objekta ili ne
    //private Integer karma = 0;

    // Za sada se po defaultu radi join column pa se pravi vezna tabela.
    //CascadeType.REMOVE, ako obrisem post, sve reakcije se brisu.Ima smisla.
//    @OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
//    private Set<Reaction> reactions;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "community_id", nullable = false)
    private Community community;

//    @OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
//    private Set<Comment> comments;

//    @OneToMany(mappedBy = "post", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
//    private Set<Report> reports;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flairId", nullable = true)
    private Flair flair;

}
