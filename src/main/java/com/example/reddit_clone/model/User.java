package com.example.reddit_clone.model;

import com.example.reddit_clone.model.Banned;
import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.model.enums.Roles;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;


@Data
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "Role")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String email;

    @Column()
    private String avatar;

    @Column(nullable = false)
    private Date registrationDate;

    @Column()
    private String description;

    @Column(nullable = false)
    private String displayName;

    @Column(nullable = false)
    private Boolean active;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Roles roles;

//    @JsonIgnore
//    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
//    private Set<Post> posts;
//
//    @JsonIgnore
//    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
//    private Set<Comment> comments;
//
//    @JsonIgnore
//    @OneToMany(mappedBy = "by", fetch = FetchType.LAZY)
//    private Set<Banned> banned;

    // Mada ne mora ali mozda dodati definiciju veze sa Reaction klasom, za sada je samo na onoj drugoj strani.

}
