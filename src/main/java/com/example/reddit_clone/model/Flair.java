package com.example.reddit_clone.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
public class Flair {

    // Represents tag which is used by community or post.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Getter @Setter
    private int flairId;

    @Column(unique = true, nullable = false)
    @Getter @Setter
    private String name;

    @Column(nullable = false)
    @Getter @Setter
    private Boolean active;

//    @OneToMany(mappedBy = "flair", fetch = FetchType.LAZY)
//    private Set<Post> post;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "flairs_communities",
            joinColumns = @JoinColumn(name = "flair_id"),
            inverseJoinColumns = @JoinColumn(name = "community_id")
    )
    @Getter @Setter
    private Set<Community> communities;

}
