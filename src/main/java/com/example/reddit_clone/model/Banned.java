package com.example.reddit_clone.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
public class Banned {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int bannedId;

    @Column(nullable = false)
    private Date creationDate;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User by;

    @ManyToOne
    @JoinColumn(name = "community_id", nullable = true)
    private Community community;

    @Column(name = "bannedReason", nullable = false)
    private String bannedReason;

    @Column(name = "active", nullable = false)
    private Boolean active;

}
