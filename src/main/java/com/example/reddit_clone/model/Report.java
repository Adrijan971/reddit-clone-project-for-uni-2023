package com.example.reddit_clone.model;

import com.example.reddit_clone.model.enums.ReportReason;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int reportId;

    @Column(nullable = false)
    private ReportReason reason;

    @Column(nullable = false)
    private Date creationDate;

    @OneToOne
    @JoinColumn(name = "user_id",nullable = false)
    private User byUser;

    @ManyToOne
    @JoinColumn(name = "post_id",nullable = true)
    private Post post;

    @ManyToOne
    @JoinColumn(name = "comment_id",nullable = true)
    private Comment comment;

    @Column(nullable = false)
    private boolean accepted;

}
