package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.model.Reaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ReactionRepository extends JpaRepository<Reaction, Integer> {

    Optional<Reaction> findFirstByReactionId(Integer id);

    List<Reaction> findByPostId(Integer id);

    @Query("SELECT r FROM Reaction r WHERE r.user.id = :userId AND r.post.id IN :postIds")
    List<Reaction> findByUserIdAndPostIdIn(@Param("userId") Integer userId, @Param("postIds") List<Integer> postIds);

    @Query("SELECT r FROM Reaction r WHERE r.user.id = :userId AND r.comment.id IN :commentIds")
    List<Reaction> findByUserIdAndCommentIdIn(@Param("userId") Integer userId, @Param("commentIds") List<Integer> commentIds);
}
