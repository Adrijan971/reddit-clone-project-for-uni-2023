package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Integer> {

    Optional<Post> findFirstById(Integer id);

}
