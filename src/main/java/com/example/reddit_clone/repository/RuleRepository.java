package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Rule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuleRepository extends JpaRepository<Rule, Integer> {
}
