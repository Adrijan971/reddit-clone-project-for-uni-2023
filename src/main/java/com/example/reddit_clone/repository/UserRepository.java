package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findFirstByUsername(String username);

    Optional<User> findFirstById(Integer id);

}
