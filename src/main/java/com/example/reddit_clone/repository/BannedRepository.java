package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Banned;
import com.example.reddit_clone.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;

public interface BannedRepository extends JpaRepository<Banned, Integer> {

    Optional<Banned> findFirstByBannedId(Integer id);



}
