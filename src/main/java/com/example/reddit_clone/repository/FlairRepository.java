package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.Post;
import com.example.reddit_clone.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FlairRepository extends JpaRepository<Flair, Integer> {

    Optional<Flair> findFirstByFlairId(Integer id);

}
