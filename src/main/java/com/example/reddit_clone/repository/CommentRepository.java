package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Comment;
import com.example.reddit_clone.model.Flair;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

    Optional<Comment> findFirstByCommentId(Integer id);


    List<Comment> findByUserId(Integer integer);
}
