package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ReportRepository extends JpaRepository<Report, Integer> {

    Optional<Report> findFirstByReportId(Integer id);

}
