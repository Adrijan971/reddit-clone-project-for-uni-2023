package com.example.reddit_clone.repository;

import com.example.reddit_clone.model.Community;
import com.example.reddit_clone.model.Flair;
import com.example.reddit_clone.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CommunityRepository extends JpaRepository<Community, Integer> {

    Optional<Community> findFirstByCommunityId(Integer id);

    // JPQL query to fetch communities with a number of posts between minCount and maxCount
    @Query("SELECT c FROM Community c WHERE SIZE(c.posts) BETWEEN :minCount AND :maxCount")
    List<Community> findCommunitiesWithPostsCountInRange(@Param("minCount") int minCount, @Param("maxCount") int maxCount);



}
